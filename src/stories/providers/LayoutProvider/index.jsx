import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  MenuItem,
  ListItemText,
  Divider,
  MenuList,
  Drawer,
  ListItem,
  Typography,
  Avatar,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { locationSelector } from 'containers/App/selectors';
import { userSelector } from 'containers/Auth/selectors';
import { logout } from 'containers/Auth/actions';
import { bindActionCreators } from 'utils';
import { createSelector } from 'reselect';
import styles from './styles';

const menuItems = [
  {
    label: 'menu.companies',
    path: '/',
  },
  {
    label: 'menu.littlerUsers',
    path: '/users',
  },
  {
    label: 'menu.policiesManagement',
    path: '/policies-management',
  },
  {
    label: 'menu.changePassword',
    path: '/change-password',
  },
];

@styles
@connect(createSelector(
  locationSelector,
  userSelector,
  (location, user) => ({
    location, user,
  }),
), bindActionCreators({ logout }))
export default class LayoutProvider extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    classes: PropTypes.object,
    user: PropTypes.object,
    actions: PropTypes.object,
    location: PropTypes.object,
  };

  static defaultProps = {
    location: {
      path: '/',
    },
    actions: {},
    user: {},
    classes: {},
  };

  render() {
    const {
      classes,
      children,
      location,
      actions,
      user,
    } = this.props;

    const drawer = (
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <ListItem
          className={classes.userProfileItem}
        >
          <Avatar className={classes.avatar}>
            {user.firstName[0]}{user.lastName[0]}
          </Avatar>
          <ListItemText
            disableTypography
            primary={(
              <Typography
                type="subheading"
                className={classes.subheading}
              >
                {user.firstName} {user.lastName}
              </Typography>
            )}
          />
        </ListItem>

        <Divider />
        <MenuList>
          {menuItems.map(item => (
            <MenuItem
              key={item.path}
              selected={location.pathname === item.path}
              classes={{
                root: classes.menuItemRoot,
                selected: classes.menuItemSelected,
              }}
            >
              <ListItemText
                disableTypography
                primary={(
                  <Link to={item.path} className={classes.menuLink}>
                    <Typography
                      type="subheading"
                      className={
                        location.pathname === item.path
                          ? classes.subheadingSelected
                          : classes.subheading
                      }
                    >
                      <FormattedMessage id={item.label} />
                    </Typography>
                  </Link>
                )}
              />
            </MenuItem>
          ))}
          <MenuItem
            onClick={actions.logout}
            classes={{
              root: classes.menuItemRoot,
              selected: classes.menuItemSelected,
            }}
          >
            <ListItemText
              disableTypography
              primary={(
                <Typography type="subheading" className={classes.subheading}>
                  <FormattedMessage id="menu.logout" />
                </Typography>
              )}
            />
          </MenuItem>
        </MenuList>
      </Drawer>
    );


    return (
      <div className={classes.root}>
        <div className={classes.appFrame}>
          {drawer}
          <div className={classes.content}>
            {children}
          </div>
        </div>
      </div>
    );
  }
}
