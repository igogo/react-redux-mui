import { withStyles } from '@material-ui/core/styles';

export default withStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  userProfileItem: {
    padding: theme.spacing.unit * 3,
  },
  appFrame: {
    minHeight: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    width: `calc(100% - ${theme.app.drawerWidth}px)`,
  },
  'appBar-left': {
    marginLeft: theme.app.drawerWidth,
  },
  'appBar-right': {
    marginRight: theme.app.drawerWidth,
  },
  drawerPaper: {
    position: 'relative',
    width: theme.app.drawerWidth,
    backgroundColor: theme.palette.background.secondary,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
  menuLink: {
    textDecoration: 'none',
  },
  subheading: {
    color: theme.palette.common.white,
  },
  subheadingSelected: {
    color: theme.palette.primary.main,
  },
  menuItemRoot: {
    height: '25px',
    paddingTop: '5px',
    paddingBottom: '5px',
    marginTop: '10px',
    marginBottom: '5px',
    borderLeft: '4px solid transparent',
  },
  menuItemSelected: {
    borderLeftColor: theme.palette.primary.main,
    color: theme.palette.primary.main,
    backgroundColor: `${theme.palette.background.secondary} !important`,
  },
  avatar: {
    fontSize: 12,
    fontWeight: theme.typography.fontWeightMedium,
    width: 34,
    height: 34,
    backgroundColor: '#F5A623',
    color: theme.palette.common.white,
  },
}));
