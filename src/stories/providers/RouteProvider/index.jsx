import React from 'react';
import { connect } from 'react-redux';
import { get as getCookie } from 'js-cookie';
import PropTypes from 'prop-types';
import { Route, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import { isLoggedInSelector, loginLoadingSelector } from 'containers/Auth/selectors';
import { createSelector } from 'reselect';
import LoginContainer from 'containers/Auth/LoginContainer';
import ForgotPasswordContainer from 'containers/Auth/ForgotPasswordContainer';
import MainContainer from 'containers/Company/CompanyListContainer/index';
import { restore } from 'containers/Auth/actions';
import NotFound from 'containers/NotFound/index';
import CircularProgress from '@material-ui/core/es/CircularProgress/CircularProgress';
import { bindActionCreators } from 'utils';
import AuthProvider from 'providers/AuthProvider';
import PrivateRoute from 'components/PrivateRoute';

@connect(createSelector(
  isLoggedInSelector,
  loginLoadingSelector,
  (isLoggedIn, loading) => ({
    isLoggedIn, loading,
  }),
), bindActionCreators({ restore }))
export default class RouteProvider extends React.Component {
  static propTypes = {
    isLoggedIn: PropTypes.bool,
    loading: PropTypes.bool,
    actions: PropTypes.object,
    history: PropTypes.object.isRequired,
  };

  static defaultProps = {
    isLoggedIn: false,
    loading: false,
    actions: {},
  };

  componentDidMount() {
    const { isLoggedIn, actions } = this.props;
    if (isLoggedIn) return;

    const authToken = getCookie('authToken');
    if (authToken) {
      actions.restore(authToken, '/');
    }
  }

  render() {
    const { isLoggedIn, loading, history } = this.props;

    return loading
      ? (
        <AuthProvider>
          <CircularProgress size={100} />
        </AuthProvider>
      )
      : (
        <ConnectedRouter history={history}>
          <Switch>
            <PrivateRoute
              exact
              path="/"
              isLoggedIn={isLoggedIn}
              component={MainContainer}
            />

            <Route path="/login" component={LoginContainer} />
            <Route path="/forgot-password" component={ForgotPasswordContainer} />

            <Route component={NotFound} />
          </Switch>
        </ConnectedRouter>
      );
  }
}
