import React from 'react';
import PropTypes from 'prop-types';
import Grid from '@material-ui/core/Grid/Grid';
import styles from './styles';

const AuthProvider = ({ children, classes }) => (
  <Grid container className={classes.root}>
    <Grid item xs={12}>
      <Grid
        container
        direction="column"
        alignItems="center"
        justify="center"
        className={classes.container}
      >
        <Grid
          item
          className={classes.centeredItem}
        >
          {children}
        </Grid>
      </Grid>
    </Grid>
  </Grid>
);

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired,
  classes: PropTypes.object,
};

AuthProvider.defaultProps = {
  classes: {},
};

export default styles(AuthProvider);
