import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  '@global': {
    body: {
      height: '100%',
    },
    html: {
      height: '100%',
    },
    '#app': {
      height: '100%',
    },
  },
  root: {
    flexGrow: 1,
    minHeight: '100%',
  },
  container: {
    minHeight: '100%',
    backgroundColor: theme.palette.background.secondary,
  },
  centeredItem: {
    width: '388px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
}));
