import React from 'react';
import PropTypes from 'prop-types';
import { TextField as MUITextField } from '@material-ui/core';

const SelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}) => (
  <MUITextField
    placeholder={label}
    error={!!(touched && error)}
    {...input}
    onChange={(event, index, value) => input.onChange(value)}
    {...custom}
  >
    {children}
  </MUITextField>
);

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  children: PropTypes.node.isRequired,
};

export default SelectField;
