import React from 'react';
import uuidv1 from 'uuid/v1';
import PropTypes from 'prop-types';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core';

const FormDialog = ({
  isOpen,
  title,
  children,
  handleDecline,
  handleAccept,
  acceptLabel,
  declineLabel,
  acceptButtonOptions,
  declineButtonOptions,
}) => {
  const id = uuidv1();

  return (
    <Dialog
      maxWidth="xs"
      open={isOpen}
      onClose={handleDecline}
      aria-labelledby={id}
    >
      <DialogTitle id={id}>
        {title}
      </DialogTitle>
      <DialogContent>
        {children}
      </DialogContent>
      <DialogActions>
        <Button
          {...declineButtonOptions}
          onClick={handleDecline}
          color="default"
        >
          {declineLabel}
        </Button>
        <Button
          {...acceptButtonOptions}
          type="submit"
          onClick={handleAccept}
          color="primary"
        >
          {acceptLabel}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

FormDialog.propTypes = {
  title: PropTypes.node.isRequired,
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  handleDecline: PropTypes.func.isRequired,
  handleAccept: PropTypes.func.isRequired,
  acceptLabel: PropTypes.node.isRequired,
  declineLabel: PropTypes.node.isRequired,
  acceptButtonOptions: PropTypes.object,
  declineButtonOptions: PropTypes.object,
};

FormDialog.defaultProps = {
  acceptButtonOptions: {},
  declineButtonOptions: {},
};


export default FormDialog;
