import React from 'react';
import PropTypes from 'prop-types';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const TextField = ({
  input: { name, ...input },
  label,
  meta: { touched, error },
  formControlOptions,
  ...inputOptions
}) => {
  const hasError = !!(touched && error);
  const helperName = `${name}-error-text`;
  return (
    <FormControl
      error={hasError}
      aria-describedby={helperName}
      {...formControlOptions}
    >
      <InputLabel htmlFor={name}>{label}</InputLabel>
      <Input
        id={name}
        onChange={(event, index, value) => input.onChange(value)}
        {...input}
        {...inputOptions}
      />
      {hasError && (
        <FormHelperText id={helperName}>{error}</FormHelperText>
      )}
    </FormControl>
  );
};

TextField.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
  meta: PropTypes.object.isRequired,
  type: PropTypes.string,
  formControlOptions: PropTypes.object,
};

TextField.defaultProps = {
  type: 'text',
  formControlOptions: {},
};

export default TextField;
