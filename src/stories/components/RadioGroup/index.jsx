import React from 'react';
import PropTypes from 'prop-types';
import { RadioGroup as MUIRadioGroup } from '@material-ui/core';

const RadioGroup = ({ input, ...rest }) => (
  <MUIRadioGroup
    {...input}
    {...rest}
    valueSelected={input.value}
    onChange={(event, value) => input.onChange(value)}
  />
);

RadioGroup.propTypes = {
  input: PropTypes.object.isRequired,
};

export default RadioGroup;
