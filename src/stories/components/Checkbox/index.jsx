import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox as MUICheckbox } from '@material-ui/core';

const Checkbox = ({ input, label }) => (
  <MUICheckbox
    label={label}
    checked={!!input.value}
    onCheck={input.onChange}
  />
);

Checkbox.propTypes = {
  input: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired,
};

export default Checkbox;
