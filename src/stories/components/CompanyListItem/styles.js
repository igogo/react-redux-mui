import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  paper: {
    cursor: 'pointer',
    padding: theme.spacing.unit * 2,
    color: theme.palette.text.secondary,
    marginBottom: theme.spacing.unit,
  },
  inactive: {
    textTransform: 'lowercase',
    color: theme.palette.inactive,
  },
  logo: {
    width: 32,
    height: 32,
    marginRight: theme.spacing.unit * 3,
  },
  companyListRoot: {
    width: '50%',
  },
  companyListPrimary: {
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.fontSize,
    lineHeight: '16px',
    fontWeight: theme.typography.fontWeightRegular,
    color: 'rgba(0, 0, 0, 0.54)',
  },
  companyListSecondary: {
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeightRegular,
    fontSize: 16,
    lineHeight: '16px',
    color: 'rgba(0, 0, 0, 0.87)',
  },
}));
