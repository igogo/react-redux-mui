import React from 'react';
import PropTypes from 'prop-types';
import { Grid, ListItemText, Paper } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import styles from './styles';

@styles
export default class CompanyListItem extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    company: PropTypes.object.isRequired,
    onSelectCompany: PropTypes.func.isRequired,
  };

  static defaultProps = {
    classes: {},
  };

  get companyNameWithStatus() {
    const { company, classes } = this.props;
    return (
      <span>
        {company.name}
        {!company.isActive && (
          <span className={classes.inactive}>
            &nbsp;(<FormattedMessage id="company.inactive" />)
          </span>
        )}
      </span>
    );
  }

  handleClick = () => {
    const { company, onSelectCompany } = this.props;
    onSelectCompany(company.id);
  };

  render() {
    const { classes, company } = this.props;

    return (
      <Paper
        onClick={this.handleClick}
        className={classes.paper}
      >
        <Grid
          container
          wrap="nowrap"
          direction="row"
        >
          <img className={classes.logo} src={company.logo} alt={company.name} />

          <Grid
            container
            direction="row"
          >
            <ListItemText
              classes={{
                root: classes.companyListRoot,
                primary: classes.companyListPrimary,
                secondary: classes.companyListSecondary,
              }}
              primary={<FormattedMessage id="company.name" />}
              secondary={this.companyNameWithStatus}
            />

            <ListItemText
              classes={{
                root: classes.companyListRoot,
                primary: classes.companyListPrimary,
                secondary: classes.companyListSecondary,
              }}
              primary={<FormattedMessage id="company.users" />}
              secondary={company.users.length}
            />
          </Grid>
        </Grid>
      </Paper>
    );
  }
}
