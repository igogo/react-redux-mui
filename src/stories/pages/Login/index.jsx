import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import Paper from '@material-ui/core/Paper/Paper';
import Divider from '@material-ui/core/Divider/Divider';
import Typography from '@material-ui/core/Typography';
import AuthProvider from 'providers/AuthProvider';
import Snak from 'components/Snak';
import logoImage from 'resources/logo.svg';
import styles from './styles';

@styles
export default class Login extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    handleCloseSnack: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
    errorMessage: PropTypes.string,
  };

  static defaultProps = {
    errorMessage: null,
    classes: {},
  };

  render() {
    const {
      classes,
      children,
      errorMessage,
      handleCloseSnack,
    } = this.props;

    return (
      <AuthProvider>
        <img
          className={classes.logo}
          src={logoImage}
          alt="Littler"
        />

        <Divider className={classes.devider} />

        {!!errorMessage && (
          <Snak
            variant="error"
            onClose={handleCloseSnack}
            className={classes.margin}
            message={errorMessage}
          />
        )}

        <Paper className={classes.paper}>
          <Typography
            className={classes.headline}
            variant="headline"
            component="h1"
          >
            <FormattedMessage id="auth.login" />
          </Typography>
          {children}
        </Paper>
      </AuthProvider>
    );
  }
}
