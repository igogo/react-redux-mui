import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  margin: {
    margin: theme.spacing.unit,
  },
  paper: {
    width: '100%',
    padding: theme.spacing.padding,
  },
  devider: {
    margin: '32px 0',
    width: '100%',
  },
  logo: {
    width: '185px',
  },
  headline: {
    marginBottom: '18px',
  },
}));
