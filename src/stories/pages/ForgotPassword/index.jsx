import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { Typography, Paper } from '@material-ui/core';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import AuthProvider from 'providers/AuthProvider';
import styles from './styles';

@styles
export default class ForgotPassword extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    classes: {},
  };

  render() {
    const {
      classes,
      children,
    } = this.props;

    return (
      <AuthProvider>

        <Link
          className={classes.backButton}
          to="/login"
        >
          <ChevronLeft />
          <FormattedMessage id="auth.back" />
        </Link>

        <Paper className={classes.paper}>
          <Typography
            className={classes.headline}
            variant="headline"
            component="h1"
          >
            <FormattedMessage id="auth.forgotPassword" />
          </Typography>
          <Typography
            gutterBottom
            className={classes.headline}
          >
            <FormattedMessage id="auth.forgotPasswordDescription" />
          </Typography>
          {children}
        </Paper>
      </AuthProvider>
    );
  }
}
