import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  paper: {
    width: '100%',
    padding: theme.spacing.padding,
  },
  backButton: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    alignSelf: 'flex-start',
    textTransform: 'uppercase',
    marginBottom: theme.spacing.unit * 3,
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.fontSize,
    textDecoration: 'none',

    '&:hover': {
      textDecoration: 'underline',
    },
  },
  headline: {
    marginBottom: '18px',
  },
}));
