import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import {
  Fade,
  Button,
  Snackbar,
  CircularProgress,
  Grid,
} from '@material-ui/core';
import LayoutProvider from 'providers/LayoutProvider';
import CompanyListItem from 'components/CompanyListItem';
import FormDialog from 'components/FormDialog';
import styles from './styles';


@styles
export default class CompanyList extends React.Component {
  static propTypes = {
    loadingEdit: PropTypes.bool.isRequired,
    loadingList: PropTypes.bool.isRequired,
    list: PropTypes.array.isRequired,
    createForm: PropTypes.node.isRequired,
    onSelectCompany: PropTypes.func.isRequired,
    errorMessage: PropTypes.string,
    classes: PropTypes.object,
  };

  static defaultProps = {
    classes: {},
    errorMessage: null,
  };

  state = {
    open: false,
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const {
      list,
      classes,
      loadingList,
      loadingEdit,
      errorMessage,
      createForm,
      onSelectCompany,
    } = this.props;

    return (
      <LayoutProvider>
        <div className={classes.root}>
          <Grid container spacing={24}>
            <Grid item xs={12}>
              <Button
                onClick={this.handleClickOpen}
                variant="contained"
                color="primary"
              >
                <FormattedMessage id="company.createCompany" />
              </Button>
            </Grid>
            <Grid item xs={12}>
              {
                loadingList
                  ? (
                    <Grid
                      container
                      alignItems="center"
                      justify="center"
                      direction="row"
                    >
                      <CircularProgress />
                    </Grid>
                  )
                  : list.map(company => (
                    <CompanyListItem
                      key={company.id}
                      onSelectCompany={onSelectCompany}
                      company={company}
                    />
                  ))
              }
            </Grid>
          </Grid>
        </div>

        {
          !!errorMessage && (
            <Snackbar
              open
              autoHideDuration={5000}
              TransitionComponent={Fade}
              ContentProps={{
                'aria-describedby': 'company-error',
              }}
              message={(
                <span id="company-error">
                  {errorMessage}
                </span>
              )}
            />
          )
        }

        <FormDialog
          acceptButtonOptions={{
            disabled: loadingEdit,
          }}
          isOpen={this.state.open}
          handleAccept={this.handleClose}
          handleDecline={this.handleClose}
          title={<FormattedMessage id="company.createCompany" />}
          acceptLabel={<FormattedMessage id="company.create" />}
          declineLabel={<FormattedMessage id="company.cancel" />}
        >
          {createForm}
        </FormDialog>
      </LayoutProvider>
    );
  }
}
