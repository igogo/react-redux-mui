import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(() => ({
  root: {
    flexGrow: 1,
  },
}));
