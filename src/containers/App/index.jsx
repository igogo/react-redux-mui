import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import RouteProvider from 'providers/RouteProvider';
import theme from 'config/theme';


const App = ({ store, history }) => (
  <Provider store={store}>
    <IntlProvider>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <RouteProvider history={history} />
      </MuiThemeProvider>
    </IntlProvider>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default App;
