import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  submitButton: {
    marginTop: theme.spacing.unit * 3,
  },
  formGroup: {
    marginBottom: 18,
  },
  forgotPasswordButton: {
    alignSelf: 'flex-end',
    textTransform: 'uppercase',
    marginTop: 10,
    color: theme.palette.primary.main,
    fontWeight: theme.typography.fontWeightMedium,
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.fontSize,
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'underline',
    },
  },
}));
