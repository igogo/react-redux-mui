import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { createSelector } from 'reselect';
import { Field, reduxForm } from 'redux-form';
import Button from '@material-ui/core/Button';
import { bindActionCreators } from 'utils';
import { required, email, alphaNumeric } from 'utils/validators';
import CircularProgress from '@material-ui/core/CircularProgress';
import TextField from 'components/TextField';
import Login from 'pages/Login';
import { messagesSelector } from 'config/messages';
import { loginErrorSelector, loginLoadingSelector } from '../selectors';
import { login, clearAuthError } from '../actions';
import styles from './styles';

@styles
@reduxForm({
  form: 'login-form',
})
@connect(createSelector(
  loginErrorSelector,
  loginLoadingSelector,
  messagesSelector,
  (errorMessage, loading, messages) => ({
    loading,
    messages,
    errorMessage,
  }),
), bindActionCreators({ login, clearAuthError }))
export default class LoginContainer extends React.Component {
  static propTypes = {
    pristine: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired,
    messages: PropTypes.object.isRequired,
    loading: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    errorMessage: PropTypes.string,
    classes: PropTypes.object,
    handleSubmit: PropTypes.func,
  };

  static defaultProps = {
    classes: {},
    errorMessage: null,
    handleSubmit: () => {},
  };

  render() {
    const {
      errorMessage,
      pristine,
      submitting,
      messages,
      loading,
      actions,
      handleSubmit,
      classes,
    } = this.props;

    return (
      <Login
        errorMessage={errorMessage}
        handleCloseSnack={actions.clearAuthError}
      >
        <form method="post" onSubmit={handleSubmit(actions.login)} className={classes.form}>
          <Field
            fullWidth
            name="email"
            formControlOptions={{
              fullWidth: true,
              className: classes.formGroup,
            }}
            autoComplete="email"
            label={messages['auth.email']}
            component={TextField}
            validate={[required, email]}
          />

          <Field
            fullWidth
            formControlOptions={{
              fullWidth: true,
            }}
            name="password"
            type="password"
            autoComplete="current-password"
            label={messages['auth.password']}
            component={TextField}
            validate={[required, alphaNumeric]}
          />

          <Link
            className={classes.forgotPasswordButton}
            to="/forgot-password"
          >
            <FormattedMessage id="auth.forgotPassword?" />
          </Link>

          <Button
            fullWidth
            type="submit"
            className={classes.submitButton}
            disabled={pristine || submitting || loading}
            variant="contained"
            color="primary"
          >
            {
              loading
                ? <CircularProgress size={18} />
                : <FormattedMessage id="auth.login" />
            }
          </Button>
        </form>
      </Login>
    );
  }
}
