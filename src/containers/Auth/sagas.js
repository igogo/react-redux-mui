import { put, takeLatest, call } from 'redux-saga/effects';
import {
  set as setCookie,
  remove as removeCookie,
} from 'js-cookie';
import history from 'config/history';
import { actionTypes } from './reducer';

const users = [
  {
    id: 1,
    firstName: 'Ihor',
    lastName: 'Skliar',
    email: 'skliar.ihor@gmail.com',
    password: '123123',
    token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOiIxIiwiZW1haWwiOiJza2xpYXIuaWhvckBnbWFpbC5jb20ifQ.oXJL3bpprNbgFMhWdVBGK-ZTww5i_Bb9LCo-Mfz3c_M',
  },
];

const loginMock = ({ email, password }) => new Promise((resolve, reject) => {
  setTimeout(() => {
    const currentUser = users.find(user => user.email === email && user.password === password);
    if (currentUser) {
      return resolve(currentUser);
    }

    return reject(new Error('User not found'));
  }, 1000);
});

const restoreSessionMock = token => new Promise((resolve, reject) => {
  setTimeout(() => {
    const currentUser = users.find(user => user.token === token);
    if (currentUser) {
      return resolve(currentUser);
    }

    return reject(new Error('User not found'));
  }, 1000);
});

export function* restore() {
  yield takeLatest(actionTypes.LOGIN_RESTORE, function* doRestore({ payload }) {
    try {
      // const data = yield fetch('https://api.myjson.com/bins/oivjj');
      // const tickets = yield data.json();
      const user = yield restoreSessionMock(payload.token);
      yield put({ type: actionTypes.LOGIN_SUCCESS, payload: user });
      yield call(history.replace, payload.redirectTo || '/');
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      yield put({ type: actionTypes.LOGIN_FAILURE, payload: e.message });
    }
  });
}

export function* login() {
  yield takeLatest(actionTypes.LOGIN_REQUEST, function* doLogin({ payload }) {
    try {
      // const data = yield fetch('https://api.myjson.com/bins/oivjj');
      // const tickets = yield data.json();
      const user = yield loginMock(payload);
      yield call(setCookie, 'authToken', user.token);
      yield put({ type: actionTypes.LOGIN_SUCCESS, payload: user });
      yield call(history.replace, '/');
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      yield put({ type: actionTypes.LOGIN_FAILURE, payload: e.message });
    }
  });
}

export function* logout() {
  yield takeLatest(actionTypes.LOGOUT, function* doLogout() {
    try {
      yield call(removeCookie, 'authToken');
      yield call(history.replace, '/login');
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  });
}
