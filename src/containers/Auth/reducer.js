// @flow
import Immutable from 'seamless-immutable';
import { get as getCookie } from 'js-cookie';
import { createReducer } from 'utils';

export const actionTypes = {
  LOGIN_REQUEST: 'AUTH/LOGIN_REQUEST',
  LOGIN_RESTORE: 'AUTH/LOGIN_RESTORE',
  LOGIN_FAILURE: 'AUTH/LOGIN_FAILURE',
  LOGIN_SUCCESS: 'AUTH/LOGIN_SUCCESS',
  LOGIN_CLEAR_ERROR: 'AUTH/LOGIN_CLEAR_ERROR',
  LOGOUT: 'AUTH/LOGOUT',
};

export type authType = {
  isLoggedIn: boolean,
  loading: boolean,
  errorMessage: ?string,
  user: ?{},
};

const authToken = getCookie('authToken');
const initialState: authType = new Immutable({
  isLoggedIn: false,
  loading: !!authToken,
  errorMessage: null,
  user: null,
});

export default createReducer(initialState, {
  [actionTypes.LOGIN_CLEAR_ERROR]: (state: authType): authType => state.merge({
    errorMessage: null,
  }),

  [actionTypes.LOGIN_RESTORE]: (state: authType): authType => state.merge({
    user: null,
    loading: true,
    errorMessage: null,
    isLoggedIn: false,
  }),

  [actionTypes.LOGIN_REQUEST]: (state: authType): authType => state.merge({
    user: null,
    loading: true,
    errorMessage: null,
    isLoggedIn: false,
  }),

  [actionTypes.LOGIN_FAILURE]: (state: authType, errorMessage: string): authType => state.merge({
    errorMessage,
    user: null,
    loading: false,
    isLoggedIn: true,
  }),

  [actionTypes.LOGIN_SUCCESS]: (state: authType, user: {}): authType => state.merge({
    user,
    loading: false,
    isLoggedIn: true,
    errorMessage: null,
  }),

  [actionTypes.LOGOUT]: (state: authType): authType => state.set({
    isLoggedIn: false,
    loading: false,
    errorMessage: null,
    user: null,
  }),
});
