import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { createSelector } from 'reselect';
import { Field, reduxForm } from 'redux-form';
import Button from '@material-ui/core/Button';
import { bindActionCreators } from 'utils';
import { required, email } from 'utils/validators';
import TextField from 'components/TextField';
import ForgotPassword from 'pages/ForgotPassword';
import { messagesSelector } from 'config/messages';
import { login, clearAuthError } from '../actions';
import styles from './styles';

@styles
@reduxForm({
  form: 'forgot-password-form',
})
@connect(createSelector(
  messagesSelector,
  messages => ({
    messages,
  }),
), bindActionCreators({ login, clearAuthError }))
export default class AuthContainer extends React.Component {
  static propTypes = {
    classes: PropTypes.object,
    pristine: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired,
    messages: PropTypes.object.isRequired,
    submitting: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func,
  };

  static defaultProps = {
    handleSubmit: () => {},
    classes: {},
  };

  render() {
    const {
      pristine,
      submitting,
      messages,
      actions,
      handleSubmit,
      classes,
    } = this.props;

    return (
      <ForgotPassword handleCloseSnack={actions.clearAuthError}>
        <form method="post" onSubmit={handleSubmit(actions.login)} className={classes.form}>
          <Field
            fullWidth
            name="email"
            formControlOptions={{
              fullWidth: true,
              className: classes.formGroup,
            }}
            autoComplete="email"
            label={messages['auth.email']}
            component={TextField}
            validate={[required, email]}
          />

          <Button
            fullWidth
            type="submit"
            className={classes.submitButton}
            disabled={pristine || submitting}
            variant="contained"
            color="primary"
          >
            <FormattedMessage id="auth.sendMeALink" />
          </Button>
        </form>
      </ForgotPassword>
    );
  }
}
