import withStyles from '@material-ui/core/styles/withStyles';

export default withStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
  submitButton: {
    marginTop: theme.spacing.unit * 3,
  },
  formGroup: {
    marginBottom: 18,
  },
}));
