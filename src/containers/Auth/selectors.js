export const isLoggedInSelector = ({ auth }) => auth.isLoggedIn;
export const loginErrorSelector = ({ auth }) => auth.errorMessage;
export const loginLoadingSelector = ({ auth }) => auth.loading;
export const userSelector = ({ auth }) => auth.user;
