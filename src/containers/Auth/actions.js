// @flow
import { actionTypes } from './reducer';

type loginType = {
  email: string,
  password: string,
};

export const login = (loginData: loginType) => ({
  payload: loginData,
  type: actionTypes.LOGIN_REQUEST,
});

export const restore = (token: string, redirectTo: string) => ({
  payload: { token, redirectTo },
  type: actionTypes.LOGIN_RESTORE,
});

export const clearAuthError = () => ({
  type: actionTypes.LOGIN_CLEAR_ERROR,
});

export const logout = () => ({
  type: actionTypes.LOGOUT,
});
