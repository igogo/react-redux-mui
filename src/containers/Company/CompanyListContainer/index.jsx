import React from 'react';
import PropTypes from 'prop-types';
import { createSelector } from 'reselect';
import {
  companyEditLoadingSelector,
  companyErrorSelector,
  companyListLoadingSelector,
  companyListSelector,
} from 'containers/Company/selectors';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { loadCompanies } from 'containers/Company/actions';
import { messagesSelector } from 'config/messages';
import { bindActionCreators } from 'utils';
import history from 'config/history';
import CompanyList from 'pages/CompanyList';
import { required, email, number } from 'utils/validators';
import TextField from 'components/TextField';


@reduxForm({
  form: 'create-compay-form',
})
@connect(createSelector(
  companyErrorSelector,
  companyListLoadingSelector,
  companyEditLoadingSelector,
  messagesSelector,
  companyListSelector,
  (
    errorMessage,
    loadingList,
    loadingEdit,
    messages,
    list,
  ) => ({
    errorMessage,
    loadingList,
    loadingEdit,
    messages,
    list,
  }),
), bindActionCreators({ loadCompanies }))
export default class CompanyContainer extends React.Component {
  static propTypes = {
    errorMessage: PropTypes.string,
    messages: PropTypes.object,
    loadingList: PropTypes.bool,
    loadingEdit: PropTypes.bool,
    actions: PropTypes.object,
    list: PropTypes.array,
  };

  static defaultProps = {
    errorMessage: null,
    loadingList: false,
    loadingEdit: false,
    list: [],
    actions: {},
    messages: {},
  };

  componentDidMount() {
    this.props.actions.loadCompanies();
  }

  handleSelectCompany = (id) => {
    history.push(`/company/${id}`);
  };

  render() {
    const {
      list,
      loadingList,
      loadingEdit,
      errorMessage,
      messages,
    } = this.props;

    return (
      <CompanyList
        loadingEdit={loadingEdit}
        list={list}
        loadingList={loadingList}
        errorMessage={errorMessage}
        onSelectCompany={this.handleSelectCompany}
        createForm={(
          <form>
            <Field
              fullWidth
              name="name"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.companyName']}
              component={TextField}
              validate={[required]}
            />
            <Field
              fullWidth
              name="website"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.website']}
              component={TextField}
              validate={[required]}
            />
            <Field
              fullWidth
              name="contactPersonFirstName"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.contactPersonFirstName']}
              component={TextField}
              validate={[required]}
            />
            <Field
              fullWidth
              name="contactPersonLastName"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.contactPersonLastName']}
              component={TextField}
              validate={[required]}
            />
            <Field
              fullWidth
              name="contactPersonEmail"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.contactPersonEmail']}
              component={TextField}
              validate={[required, email]}
            />

            <Field
              fullWidth
              name="contactPersonPhone"
              formControlOptions={{
                fullWidth: true,
              }}
              label={messages['company.contactPersonPhone']}
              component={TextField}
              validate={[required, number]}
            />
          </form>
        )}
      />
    );
  }
}
