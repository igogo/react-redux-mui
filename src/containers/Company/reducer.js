// @flow
import Immutable from 'seamless-immutable';
import { createReducer } from 'utils';

export const actionTypes = {
  COMPANY_LIST_REQUEST: 'COMPANY/COMPANY_LIST_REQUEST',
  COMPANY_LIST_FAILURE: 'COMPANY/COMPANY_LIST_FAILURE',
  COMPANY_LIST_SUCCESS: 'COMPANY/COMPANY_LIST_SUCCESS',

  COMPANY_CREATE_REQUEST: 'COMPANY/COMPANY_CREATE_REQUEST',
  COMPANY_CREATE_FAILURE: 'COMPANY/COMPANY_CREATE_FAILURE',
  COMPANY_CREATE_SUCCESS: 'COMPANY/COMPANY_CREATE_SUCCESS',
};

export type companyType = {
  loadingList: boolean,
  loadingEdit: boolean,
  errorMessage: ?string,
  list: [],
};

const initialState: companyType = new Immutable({
  loadingList: false,
  loadingEdit: false,
  errorMessage: null,
  list: [],
});

export default createReducer(initialState, {
  [actionTypes.COMPANY_LIST_REQUEST]: (state: companyType): companyType => state.merge({
    list: [],
    loadingList: true,
    errorMessage: null,
  }),

  [actionTypes.COMPANY_LIST_FAILURE]: (state: companyType, errorMessage: string): companyType => (
    state.merge({
      errorMessage,
      list: [],
      loadingList: false,
    })
  ),

  [actionTypes.COMPANY_LIST_SUCCESS]: (state: companyType, list: []): companyType => state.merge({
    list,
    loadingList: false,
    errorMessage: null,
  }),

  [actionTypes.COMPANY_CREATE_REQUEST]: (state: companyType): companyType => state.merge({
    loadingEdit: true,
    errorMessage: null,
  }),

  [actionTypes.COMPANY_CREATE_FAILURE]: (state: companyType, errorMessage: string): companyType => (
    state.merge({
      errorMessage,
      loadingEdit: false,
    })
  ),

  [actionTypes.COMPANY_CREATE_SUCCESS]: (state: companyType, company: {}): companyType => (
    state.merge({
      list: [
        ...state.list,
        company,
      ],
      loadingEdit: false,
      errorMessage: null,
    })
  ),
});
