import { put, takeLatest } from 'redux-saga/effects';
import { actionTypes } from './reducer';

const companies = [
  {
    id: 1,
    logo: 'https://fakeimg.pl/32x32/',
    name: 'Softmax',
    isActive: true,
    website: 'https://google.com',
    contactPersonFirstName: 'Ihor',
    contactPersonLastName: 'Skliar',
    contactPersonEmail: 'skliar.ihor@gmail.com',
    contactPersonPhone: 123123123,
    users: [],
  },
  {
    id: 2,
    logo: 'https://fakeimg.pl/32x32/',
    name: 'Williamson & Co',
    isActive: true,
    website: 'https://google.com',
    contactPersonFirstName: 'Ihor',
    contactPersonLastName: 'Skliar',
    contactPersonEmail: 'skliar.ihor@gmail.com',
    contactPersonPhone: 123123123,
    users: [],
  },
  {
    id: 3,
    logo: 'https://fakeimg.pl/32x32/',
    name: 'New Wave Group',
    isActive: false,
    website: 'https://google.com',
    contactPersonFirstName: 'Ihor',
    contactPersonLastName: 'Skliar',
    contactPersonEmail: 'skliar.ihor@gmail.com',
    contactPersonPhone: 123123123,
    users: [],
  },
];

let lastId = companies[companies.length - 1].id;

const loadCompaniesMock = () => new Promise((resolve) => {
  setTimeout(() => {
    resolve(companies);
  }, 1000);
});

const createMock = data => new Promise((resolve) => {
  setTimeout(() => {
    resolve({
      ...data,
      id: ++lastId,
    });
  }, 1000);
});

export function* getCompanies() {
  yield takeLatest(actionTypes.COMPANY_LIST_REQUEST, function* doCompanyRequest() {
    try {
      // const data = yield fetch('https://api.myjson.com/bins/oivjj');
      // const tickets = yield data.json();
      const companiesList = yield loadCompaniesMock();
      yield put({ type: actionTypes.COMPANY_LIST_SUCCESS, payload: companiesList });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      yield put({ type: actionTypes.COMPANY_LIST_FAILURE, payload: e.message });
    }
  });
}

export function* createCompany() {
  yield takeLatest(actionTypes.COMPANY_CREATE_REQUEST, function* doCreatingCompany({ payload }) {
    try {
      // const data = yield fetch('https://api.myjson.com/bins/oivjj');
      // const tickets = yield data.json();
      const createdCompany = yield createMock(payload);
      yield put({ type: actionTypes.COMPANY_CREATE_SUCCESS, payload: createdCompany });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
      yield put({ type: actionTypes.COMPANY_CREATE_FAILURE, payload: e.message });
    }
  });
}
