export const companyErrorSelector = ({ company }) => company.errorMessage;
export const companyListLoadingSelector = ({ company }) => company.loadingList;
export const companyEditLoadingSelector = ({ company }) => company.loadingEdit;
export const companyListSelector = ({ company }) => company.list;
