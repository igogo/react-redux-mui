// @flow
import { actionTypes } from './reducer';

export const loadCompanies = (list: []) => ({
  payload: list,
  type: actionTypes.COMPANY_LIST_REQUEST,
});
