import 'babel-polyfill';
import 'typeface-roboto';
import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from 'config/configureStore';
import App from 'containers/App';
import { DEFAULT_LOCALE } from 'config/constants';
import messages from 'config/messages';

const { store, history } = configureStore({
  intl: {
    messages,
    locale: DEFAULT_LOCALE,
  },
});

ReactDOM.render(
  <App store={store} history={history} />,
  document.getElementById('app'),
);
