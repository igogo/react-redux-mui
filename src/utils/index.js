import { bindActionCreators as reduxBindActionCreators } from 'redux';

export const createReducer = (initialState, reducerMap) => (state = initialState, action) => {
  if (typeof reducerMap[action.type] === 'function') {
    return reducerMap[action.type](state, action.payload);
  }
  return state;
};

export const bindActionCreators = actionCreators => dispatch => ({
  actions: reduxBindActionCreators(actionCreators, dispatch),
});
