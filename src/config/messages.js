import authMessages from 'containers/Auth/messages/en';
import appMessages from 'containers/App/messages/en';
import companyMessages from 'containers/Company/messages/en';

export const localeSelector = ({ intl }) => intl.locale;
export const messagesSelector = ({ intl }) => intl.messages;

export default {
  ...appMessages,
  ...authMessages,
  ...companyMessages,
};
