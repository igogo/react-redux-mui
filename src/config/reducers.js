import { routerReducer as router } from 'react-router-redux';
import { intlReducer as intl } from 'react-intl-redux';
import { reducer as form } from 'redux-form';
import auth from 'containers/Auth/reducer';
import company from 'containers/Company/reducer';

export default {
  intl,
  auth,
  router,
  form,
  company,
};
