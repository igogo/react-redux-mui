import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import sagaMiddleware from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import sagas from './sagas';
import reducers from './reducers';
import history from './history';
import { IS_DEV } from './constants';

export default (initialState) => {
  const saga = sagaMiddleware();

  const middlewares = [];

  middlewares.push(routerMiddleware(history));
  middlewares.push(saga);

  if (IS_DEV) {
  // eslint-disable-next-line global-require
    const { logger } = require('redux-logger');

    middlewares.push(logger);
  }

  // eslint-disable-next-line no-undef,no-underscore-dangle
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const enhancer = composeEnhancers(applyMiddleware(...middlewares));
  const store = createStore(
    combineReducers(reducers),
    initialState,
    enhancer,
  );

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('./index'); // eslint-disable-line global-require
      store.replaceReducer(nextRootReducer);
    });
  }

  saga.run(sagas);

  return { store, history };
};
