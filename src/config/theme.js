import { createMuiTheme } from '@material-ui/core/styles/index';

export default createMuiTheme({
  app: {
    drawerWidth: 256,
  },
  spacing: {
    padding: 32,
  },
  palette: {
    inactive: '#D0021B',
    primary: {
      main: '#69923A',
    },
    background: {
      default: '#EEEEEE',
      secondary: '#2C2C2C',
    },
    divider: 'rgba(255, 255, 255, 0.12)',
  },
});
