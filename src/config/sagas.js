import { all, fork } from 'redux-saga/effects';
import * as authSagas from 'containers/Auth/sagas';
import * as companySagas from 'containers/Company/sagas';

const forkSagas = sagas => Object.values(sagas).map(saga => fork(saga));

export default function* root() {
  yield all([
    ...forkSagas(authSagas),
    ...forkSagas(companySagas),
  ]);
}
